#!/bin/sh

WARNING='\033[1;31m'
INPUT='\033[1;33m'
INFO='\033[1;32m'
NC='\033[0m'

CONTEST_DIR="/tmp/contests"
FUSE_DIR="ej-fuse"
CWD=$(pwd)

echo -e "${WARNING}You need to install git, libfuse-dev, libcurl4-openssl-dev, libssl-dev for run this script${NC}"

echo -e "${INFO}Clone git repo${NC}"
git clone https://github.com/blackav/ejudge-fuse.git $FUSE_DIR
cd $FUSE_DIR/src && make

echo -en "${INPUT}Please enter your ejudge login: ${NC}"
read login
echo -en "${INPUT}Please enter ejudge password: ${NC}"
read -s password

echo -e "\n${INFO}Make dir for mounting ejudge${NC}"
mkdir -p $CONTEST_DIR

echo -e "${INFO}Mounting ejudge${NC}"
if ! ./ejudge-fuse --user $login --password $password --url https://unicorn.ejudge.ru/cgi-bin/ $CONTEST_DIR -o use_ino; then
    echo -e "${WARNING}Problem with ejudge mounting. Please check your login and password${NC}"
    cd $CWD
    rm -rf $CONTEST_DIR $FUSE_DIR
    exit
fi

echo -e "${INFO}Grab all rejects from ejudge${NC}"
cd $CWD
python3 parse.py $CONTEST_DIR

echo -e "${INFO}Clean all${NC}"
fusermount -u $CONTEST_DIR
rm -rf $FUSE_DIR $CONTEST_DIR

echo -e "${INFO}Your rejects info in file rejects.434${NC}"
